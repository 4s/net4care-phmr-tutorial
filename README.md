Net4Care PHMR Tutorial
======================

Version 2, April 2015. Updated to use version 0.0.6 of the Net4Care
builder library, and with simple QFDD demo.

*Henrik Bærbak Christensen, Department of Computer Science, University
 of Aarhus.*

Goal
----

To demonstrate how to use the Net4Care PHMR builder module to

  * Create a SimpleClinicalDocument instance and populate it with
    relevant administrative and medical information to form a tele
    medical report of a set of measurements.

  * Use the provided DanishPHMRBuilder to generate a correctly
    formatted PHMR XML document that obeys the Danish standard

  * Demonstrate a few other builder types, and how to design your own.

Version 2

To demonstrate the Net4Care QFDD builder (work in progress!) to
  
  * Create a simple QFDD document and output the XML

Prerequisites
---

You will need to install

  * Java 1.7
  * Ant 1.8+
  * Ivy 2+

Execution
---

If your Java, Ant, and Ivy environment is properly setup, you just

`ant demo`

This will compile and run the PHMR demo which outputs a lot of stuff.

`ant demo2`

This will demonstrate the **work in progress** on the QFDD building.

Code Walkthrough for PHMR
----

The tutorial code is all in the Java source file `DemoPHMR.java`.

It consists of a (long) main method that demonstrate most of the
feature set of this module. It basically is divided into two parts

  1. Creating the SimpleClinicalDocument instance. This is Java object
  that encapsulates the "dynamic" parts of a CDA: the author, the
  custodian, the patient, the measurements, etc. Heavy use of the
  Joshua Bloch's builder pattern is used to avoid long constructors
  with a zillion of String parameters.

  2. The PHMR generation phase. This uses the Builder design pattern
  from the original Gang of Four book.

Creating the SimpleClinicalDocument
---

Complex data types like patient information is done by first creating
the objects holding person and address data, and next insert it into
the SimpleClinicalDocument using `set(...)` method.

Example:

    // Step 1: Define the address
    AddressData kajsAddress = new AddressData.AddressBuilder("9800", "Hjørring").
        setCountry("Denmark").
        addAddressLine("Søndermarksvej 12").
        setUse(AddressData.Use.HomeAddress).build();
    
    // Next define the person and link it to the address
    PersonIdentity kaj = 
        new PersonIdentity.PersonBuilder("Hansen").
        addGivenName("Kaj").
        setGender(Gender.Male).
        setPersonID("1711001357").
        setAddress(kajsAddress).
        addTelecom(AddressData.Use.HomeAddress, "tel","98123456").
        setBirthTime(2000, Calendar.NOVEMBER, 17).build();
    
    System.out.println(" -> Built the patient: " + kaj.toString());
    
    cda.setPatient(kaj);


Generating the PHMR
----

To generate PHMR you first create a builder specifically for the PHMR,
next ask the SimpleClinicalDocument instance to use this builder in
its construction phase, and finally you request the generated output
(the XML document) from the builder.

Example:

    // Create a builder for PHMR as XML
    DanishPHMRBuilder phmrBuilder = new DanishPHMRBuilder();
    // Ask the cda to construct a representation of itself using this builder
    cda.construct(phmrBuilder);
    
    // Extract the actual XML DOM 
    Document phmrAsXML = phmrBuilder.getDocument();
    // and the String representation
    String phmrAsString = phmrBuilder.getDocumentAsString();


Note that DanishPHMRBuilder is capable of generating both the Java DOM
representation as well as formatted and indented String
representation.

Regarding the QFDD
---

This is very much work in progress, but the basic template is the same
as in the PHMR demo: Populate the model, generate the XML, and output
it to the shell.