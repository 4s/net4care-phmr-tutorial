package phmr.demo;

import java.util.*;

import org.net4care.core.Section;
import org.net4care.phmr.builders.*;
import org.net4care.phmr.codes.NPU;
import org.net4care.phmr.model.*;
import org.net4care.phmr.model.PersonIdentity.Gender;
import org.w3c.dom.*;

/** A simple usage demo of the API of the Net4Care
 * PHMR builder.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University.
 *
 */

public class DemoPHMR {
  public static void main(String[] args) {
    System.out.println( "*** Welcome to the PHMR Builder demo ***" );

    // REMEMBER: To set your editor's encoding to UTF-8 to get
    // Danish characters properly across!
    
    // To build PHMR, we create a 'SimpleClinicalDocument' and
    // instantiate it as DanishPHMRModel(). This object has a
    // large set of 'setter' methods which is used to populate
    // the data structure with the properties needed for the
    // particular tele medical document we want to create.
    
    SimpleClinicalDocument cda = new DanishPHMRModel();

    // Define the time of when the document has been created
    Calendar danishCalendar = Calendar.getInstance(new Locale("da-DK"));
    danishCalendar.set(2014, 8, 8, 14, 23, 00);
    Date documentCreationTime = danishCalendar.getTime();
    
    cda.setEffectiveTime(documentCreationTime);

    // Set the document version as part of the header
    cda.setDocumentVersion("2358344", 1);
    
    // ===================================================== Patient
    
    // Next, define the identity of the patient this PHMR describes.
    // This involves defining the address and person identity as
    // two data types:
    
    // Step 1: Define the address
    AddressData kajsAddress = new AddressData.AddressBuilder("9800", "Hjørring").
        setCountry("Denmark").
        addAddressLine("Søndermarksvej 12").
        setUse(AddressData.Use.HomeAddress).build();
    
    // Next define the person and link it to the address
    PersonIdentity kaj = 
        new PersonIdentity.PersonBuilder("Hansen").
        addGivenName("Kaj").
        setGender(Gender.Male).
        setPersonID("1711001357").
        setAddress(kajsAddress).
        addTelecom(AddressData.Use.HomeAddress, "tel","98123456").
        setBirthTime(2000, Calendar.NOVEMBER, 17).build();
    
    System.out.println(" -> Built the patient: " + kaj.toString());
    
    cda.setPatient(kaj);
    
    // ======================================================= Author, Custodian, Authenticator
    
    // In the Danish PHMR for tele medicine, it has been decided that
    // the authenticator is also the author of the document. So,
    // we create a clinician 'Susanne Madsen' from hjertemedicinsk
    // afdeling, Svendborg sygehus.
    
    // First, create the address of the organization
    AddressData hjertemedicinskAddress =
            new AddressData.AddressBuilder("5700", "Svendborg").
            addAddressLine("Hjertemedicinsk afdeling B").
            addAddressLine("Valdemarsgade 53").
            setCountry("Danmark").
            setUse(AddressData.Use.WorkPlace).
            build();

    System.out.println(" -> Built the organization's address: " + hjertemedicinskAddress.toString() );

    // Next, create the organization at that address
    OrganizationIdentity svendborgHjerteMedicinskAfdeling  =
            new OrganizationIdentity.OrganizationBuilder("88878685").
                setName("Odense Universitetshospital - Svendborg sygehus").
                setAddress(hjertemedicinskAddress).
                addTelecom(AddressData.Use.WorkPlace, "tel","65223344").
                build();
    
    System.out.println(" -> Built the organization: " + svendborgHjerteMedicinskAfdeling.toString() );
    
    // Finally, create the clinician in that organization
    PersonIdentity susanneMadsen = new PersonIdentity.PersonBuilder("Madsen").
    		addGivenName("Susanne").build();
   
    // Set the author
    cda.setAuthor(svendborgHjerteMedicinskAfdeling, susanneMadsen, documentCreationTime);
    // And custodian organization
    cda.setCustodian(svendborgHjerteMedicinskAfdeling);
    // And authenticator, Susanne Madsen again, at somewhat later time
    danishCalendar.set(2014, 8, 9, 16, 11, 21);
    Date authenticationTime = danishCalendar.getTime();
    cda.setAuthenticator(svendborgHjerteMedicinskAfdeling, susanneMadsen, authenticationTime);
    
    // ================================================================= Service interval
    // Define the time interval this PHMR report measurements for
    danishCalendar.set(2014, 8, 7, 12, 00, 00);
    Date fromTime = danishCalendar.getTime();
    danishCalendar.set(2014, 8, 8, 12, 00, 00);
    Date toTime = danishCalendar.getTime();
    
    cda.setDocumentationTimeInterval(fromTime, toTime);
    
    // ================================================================= Measurements
    
    // Finally, we are at the point where we can describe some measurements!
    
    // a) when was the measurement made
    danishCalendar.set(2014, 8, 7, 13, 23, 00);
    Date timeOfMeasurement1 = danishCalendar.getTime();
    
    // b) what was the context - here the patient himself made it and manually
    // entered the measured weight into the home device
    Context context = new Context(Context.ProvisionMethod.TypedByCitizen, Context.PerformerType.Citizen);
    
    
    // c) and finally the measurement itself - here we use the convenience methods defined in
    // the NPU class that knows common encodings in the NPU terminology
    Measurement weightAtTime1 = NPU.createWeight("77.3", timeOfMeasurement1, context);
    System.out.println(" -> Built a measurement: " + weightAtTime1.toString() );
    
    cda.addResult(weightAtTime1);
    
    // Also - define the device type used for making the measurement
    MedicalEquipment equipment = new MedicalEquipment.MedicalEquipmentBuilder().
        setMedicalDeviceCode("EPQ12225").
        setMedicalDeviceDisplayName("Weight").
        setManufacturerModelName("Manufacturer: AD Company / Model: 6121ABT1").
        setSoftwareName("SerialNr: 6121ABT1-987 Rev. 3 / SW Rev. 20144711").
        build();
    System.out.println(" -> Built a device: " + equipment.toString() );

    cda.addMedicalEquipment(equipment);

    // ================================================================ Generation of PHMR
    
    // All the above steps just populate the 'cda' instance and you can use that
    // as a convenient container of data. To build the actual XML, you construct a builder,
    // using the classic Builder design pattern.
    
    // Create a builder for PHMR as XML
    DanishPHMRBuilder phmrBuilder = new DanishPHMRBuilder();
    // Ask the cda to construct a representation of itself using this builder
    cda.construct(phmrBuilder);
    
    // Extract the actual XML DOM 
    Document phmrAsXML = phmrBuilder.getDocument();
    // and the String representation
    String phmrAsString = phmrBuilder.getDocumentAsString();
    
    // Demonstrate (very crude) use of the built XML document
    System.out.println("=========== The XML Document =================================");
    System.out.println("Root node name: " + phmrAsXML.getDocumentElement().getNodeName());
    NodeList obsList = phmrAsXML.getElementsByTagName("value");
    System.out.println("Number of nodes with tag 'value': " + obsList.getLength());
    for (int i = 0; i < obsList.getLength(); i++) {
      Node nNode = obsList .item(i);
      for (int j = 0; j < nNode.getAttributes().getLength(); j++) {
        System.out.println(" Attribute " + j + ": " + nNode.getAttributes().item(j));
      }
    }
    
    // The DanishPHMRBuilder will also build a string representation of the XML
    System.out.println("=========== The String representation ========================");
    System.out.println(phmrAsString);
    
    // There is also a PlainStringBuilder which is useful for debugging
    System.out.println("=========== Using PlainStringBuilder =========================");
    PlainStringBuilder stringBuilder = new PlainStringBuilder();
    cda.construct(stringBuilder);
    
    System.out.println(stringBuilder.getResult());
    
    // Demo of home-grown builder, the builder code is just below this. It just
    // counts number of resuls and number of vital signs.
    System.out.println("=========== Using Custom Builder =============================");
    MyBuilder myBuilder = new MyBuilder();
    cda.construct(myBuilder);
    
    System.out.println("The CDA contains "+ myBuilder.getCountSections() + " sections.");
    
    System.out.println("*** End of Demo ***");
  }
}

/** A simple builder to count sections */
class MyBuilder implements ClinicalDocumentBuilder {
  
  private int countSections = 0;
  
  public int getCountSections() { return countSections; }

  @Override
  public void buildAuthorSection(OrganizationIdentity arg0,
      PersonIdentity arg1, Date arg2) {
  }

  @Override
  public void buildCustodianSection(OrganizationIdentity arg0) {
  }

  @Override
  public void buildDocumentationOf(Date arg0, Date arg1) {
    
  }


  @Override
  public void buildLegalAuthenticatorSection(OrganizationIdentity arg0,
      PersonIdentity arg1, Date arg2) {
  }

  @Override
  public void buildPatientSection(PersonIdentity arg0) {
  }


  @Override
  public void buildRootNode() {
  }

  @Override
  public void buildStructuredBodySection() {
  }

  @Override
  public void buildContext(String arg0, PersonIdentity arg1, Date arg2,
      String arg3, String arg4) {
  }

  @Override
  public void buildCopyrightSection(String arg0) {
  }

  @Override
  public void buildHeader(CDAHeaderData arg0) {
  }

  @Override
  public void buildSections(List<Section> arg0) {
    countSections = arg0.size();
  }
}
