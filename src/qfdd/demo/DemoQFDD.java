package qfdd.demo;

import java.util.*;

import org.net4care.bro.*;
import org.net4care.bro.builder.DanishQFDBuilder;
import org.net4care.core.Section;
import org.net4care.phmr.model.*;
import org.net4care.phmr.model.AddressData.Use;
import org.w3c.dom.*;

/** A simple usage demo of the API of the Net4Care
 * PHMR builder.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University.
 *
 */

public class DemoQFDD {
  public static void main(String[] args) {
    System.out.println( "*** Welcome to the QFDD Builder demo ***" );

    // REMEMBER: To set your editor's encoding to UTF-8 to get
    // Danish characters properly across!
    
    
    DanishQuestionnaireFormDefinitionDocument qfd = new DanishQuestionnaireFormDefinitionDocument();

    // Define the title
    qfd.setTitle("SPØRGESKEMA 3 OM DIN EPILEPSI");
    
    // Define the 'time'
    Calendar danishCalendar = Calendar.getInstance(new Locale("da-DK"));
    danishCalendar.set(2014, 8, 8, 14, 23, 00);
    Date documentCreationTime = danishCalendar.getTime();
    qfd.setEffectiveTime(documentCreationTime);

    // Setup Anders Andersen as authenticator
    PersonIdentity andersAndersen =
        new PersonIdentity.PersonBuilder("Andersen").
            addGivenName("Anders").
            build();
    
    AddressData nerologiskAddress =
        new AddressData.AddressBuilder("5700", "Svendborg").
        addAddressLine("Neurologisk afdeling C").
        addAddressLine("Valdemarsgade 53").
        setCountry("Danmark").
        setUse(AddressData.Use.WorkPlace).
        build();

    // 1.3 Populate with Author, Custodian, and Authenticator
    OrganizationIdentity svendborgNeurologiskAfdeling  =
        new OrganizationIdentity.OrganizationBuilder("88878685").
            setName("Odense Universitetshospital - Svendborg Sygehus").
            setAddress(nerologiskAddress).
            addTelecom(Use.WorkPlace, "tel","65112233").
            build();
    qfd.setAuthor(svendborgNeurologiskAfdeling, andersAndersen, documentCreationTime);
    qfd.setCustodian(svendborgNeurologiskAfdeling);
    
    // QFD has no authenticator! 

    // For a QFD, the patient is No Information
    PersonIdentity noPatient = new PersonIdentity.PersonBuilder().NoInformation().build();
    qfd.setPatient(noPatient);
    
    // Create copyright section
    
    qfd.addCopyright("We have all rights to this questionaire");
    
    // =============================================================== Create question sections
    QFDSection section1;
    section1 = new QFDSection("Sektion 1", "OM DETTE SKEMA:");
    section1.addObservation(
        new QFDObservationText("Medfører din epilepsi (anfald/behandling) alvorlige begrænsninger for dig?",
            "ob1", "Some-root-OID", "Some-Assigning-Authority-Name",
            "q1", "Some-Question-OID", "Some-Display-Name", "Some-CodeSystem-Name"));
    
    qfd.addQFDSection(section1);

    // ================================================================ Generation of QFDD
    
    // All the above steps just populate the 'cda' instance and you can use that
    // as a convenient container of data. To build the actual XML, you construct a builder,
    // using the classic Builder design pattern.
    
    DanishQFDBuilder qfdBuilder = new DanishQFDBuilder();
    qfd.construct(qfdBuilder);

    // Extract the actual XML DOM 
    Document qfddAsXML = qfdBuilder.getDocument();
    // and the String representation
    String qfddAsString = qfdBuilder.getDocumentAsString();


    // Demonstrate (very crude) use of the built XML document
    System.out.println("=========== The XML Document =================================");
    System.out.println("Root node name: " + qfddAsXML.getDocumentElement().getNodeName());
    NodeList obsList = qfddAsXML.getElementsByTagName("text");
    System.out.println("Number of nodes with tag 'value': " + obsList.getLength());
    
    // The builder will also build a string representation of the XML
    System.out.println("=========== The String representation ========================");
    System.out.println(qfddAsString);
    
    System.out.println("*** End of Demo ***");
  }
}

/** A simple builder to count sections */
class MyBuilder implements ClinicalDocumentBuilder {
  
  private int countSections = 0;
  
  public int getCountSections() { return countSections; }

  @Override
  public void buildAuthorSection(OrganizationIdentity arg0,
      PersonIdentity arg1, Date arg2) {
  }

  @Override
  public void buildCustodianSection(OrganizationIdentity arg0) {
  }

  @Override
  public void buildDocumentationOf(Date arg0, Date arg1) {
    
  }


  @Override
  public void buildLegalAuthenticatorSection(OrganizationIdentity arg0,
      PersonIdentity arg1, Date arg2) {
  }

  @Override
  public void buildPatientSection(PersonIdentity arg0) {
  }


  @Override
  public void buildRootNode() {
  }

  @Override
  public void buildStructuredBodySection() {
  }

  @Override
  public void buildContext(String arg0, PersonIdentity arg1, Date arg2,
      String arg3, String arg4) {
  }

  @Override
  public void buildCopyrightSection(String arg0) {
  }

  @Override
  public void buildHeader(CDAHeaderData arg0) {
  }

  @Override
  public void buildSections(List<Section> arg0) {
    countSections = arg0.size();
  }
}
